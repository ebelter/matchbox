" GENERAL
"color, colorscheme ??
syntax on
filetype plugin indent on
set backspace=2 " allows backspacing over indentation, end-of-line, and start-of-line.
set noerrorbells " damn this beep!  ;-)
set laststatus=2 " show status line?  Yes, always!
" PATHOGEN autoloader
execute pathogen#infect()
" Retrun to previous position
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

"COMPLETE
set complete=.,w,b
set completeopt=menuone,preview

" SEARCH
set ignorecase " ignore the case in search patterns
set smartcase " but not if explicitly typed uppercase so be smart! ;)
set magic " extended regular expressions

" SYNTAX
autocmd BufReadPost *.rc set syntax=sh
autocmd BufReadPost *.profile set syntax=sh
autocmd BufReadPost Dockerfile* set syntax=dockerfile

" TABS
autocmd Filetype cpp setlocal tabstop=4 shiftwidth=4 expandtab
autocmd Filetype hpp setlocal tabstop=4 shiftwidth=4 expandtab
autocmd Filetype sh setlocal tabstop=4 shiftwidth=4 expandtab
autocmd Filetype perl setlocal tabstop=4 shiftwidth=4 expandtab
autocmd Filetype ruby setlocal tabstop=2 shiftwidth=2 expandtab
autocmd Filetype erb setlocal tabstop=2 shiftwidth=2 expandtab
autocmd Filetype html setlocal tabstop=2 shiftwidth=2 expandtab
autocmd Filetype css setlocal tabstop=2 shiftwidth=2 expandtab
autocmd Filetype scss setlocal tabstop=2 shiftwidth=2 expandtab
autocmd Filetype javascript setlocal tabstop=4 shiftwidth=4 noexpandtab
autocmd Filetype python setlocal tabstop=8 shiftwidth=4 softtabstop=4 expandtab
autocmd Filetype dockerfile setlocal tabstop=2 shiftwidth=2 expandtab
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

" SWAP/BACKUP
set backupdir=$HOME/tmp
set directory=$HOME/tmp

" MAPPINGS
nmap <CR> o
