# colors
# 1=bold 2=dim 3=italics 4=underline 5=blink 6=normal 7=flip fg/bg 8=hidden 9=normal
FG_BLACK="30;0m"
FG_RED="31;6m"
FG_GREEN="32;6m"
FG_YELLOW="33;6m"
FG_BLUE="34;6m"
FG_VIOLET="35;6m"
FG_CYAN="36;2m"
FG_WHITE="37;2m"
FG_GREY="38;05;7m"
