# .zshrc

# rcs
rc_path="${HOME}/.zshrc.d"
if [ -d "${rc_path}" -a -r "${rc_path}" -a -x "${rc_path}" ]; then
    for rc in ${rc_path}/*.sh; do
        . "${rc}"
    done
    unset rc
fi
unset rc_path

# aliases
if [ -f "${HOME}/.bash_alias" ]; then
    . "${HOME}/.bash_alias"
fi
