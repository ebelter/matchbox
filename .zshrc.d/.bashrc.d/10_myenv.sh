# environment

# add bin and .bin to path
for bin in bin .bin; do
    bin_dir="${HOME}/${bin}"
    if [[ ":${PATH}:" != *":${bin_dir}:"* ]] && [ -d "${bin_dir}" ]; then
        export "PATH=${bin_dir}:${PATH}"
    fi
    unset bin_dir
done

# history
export HISTTIMEFORMAT="%h %d - %H:%M:%S  "
export HISTCONTROL=ignoredups:erasedups  # no duplicate entries
export HISTSIZE=100000                   # big big history
export HISTFILESIZE=100000               # big big history
shopt -s histappend                      # append to history, don't overwrite it

# editors
export TMPDIR="/tmp"
export EDITOR=/usr/bin/vim
export LESS="-R"
export PAGER="/usr/bin/env less -R"

# all things vi
set -o vi; 

# ignore case on files and dirs
bind "set completion-ignore-case On";

# prompt fancy af
if [[ -n "${TERM}" ]] && [[ "${TERM}" != 'dumb' ]]; then
    PS1="
 ( \[\e[$FG_GREEN\]\w\[\e[$FG_BLACK\] as \[\e[$FG_YELLOW\]\u\[\e[$FG_BLACK\] @ \[\e[$FG_VIOLET\]\h\[\e[$FG_BLACK\] )
 \[\e[$FG_BLUE\]$\[$(tput sgr0)\] "
fi
