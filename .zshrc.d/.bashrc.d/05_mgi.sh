# MGI .bashrc

if [ -z "${MGI_NO_GAPP}" ]; then # ok to source GAPP bashrc
    [ -f /gapp/noarch/share/login/gapp.bashrc ]  && . /gapp/noarch/share/login/gapp.bashrc
else
    [ -f /opt/lsf9/conf/profile.lsf ] && . /opt/lsf9/conf/profile.lsf
fi
