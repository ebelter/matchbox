# environment

# add bin and .bin to path
for bin in bin .bin; do
    bin_dir="${HOME}/${bin}"
    if [[ ":${PATH}:" != *":${bin_dir}:"* ]] && [ -d "${bin_dir}" ]; then
        export "PATH=${bin_dir}:${PATH}"
    fi
    unset bin_dir
done

# history
export HISTTIMEFORMAT="%h %d - %H:%M:%S  "
export HISTSIZE=100000                   # big big history
export HISTFILESIZE=100000               # big big history
setopt EXTENDED_HISTORY
setopt HIST_IGNORE_ALL_DUPS

# editors
export TMPDIR="/tmp"
export EDITOR=/usr/bin/vim
export LESS="-R"
export PAGER="/usr/bin/env less -R"

# all things vi
bindkey -v

# reverse search
# bindkey -M vicmd # see vim key bindings
bindkey '^R' history-incremental-search-backward

# case insensitive path-completion
autoload -Uz +X compinit && compinit
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
zstyle ':completion:*' menu select

# prompt fancy af
if [[ -n "${TERM}" ]] && [[ "${TERM}" != 'dumb' ]]; then
    PROMPT="%F{FFF} ( %F{028}%~% %F{FFF} as %F{220}%n %F{FFF} @ %F{005}%m% %F{FFF} )
 %F{020}$ %F{FFF}"
fi
