#get-display.bash
#!/bin/bash
set -e

declare display
function get_display {
    if [ -z "${DISPLAY}" ] || [ "${DISPLAY}" == ':0.0' ]; then
        xhost +
        ip=$(/sbin/ifconfig eth0 | grep inet | awk '{print $2}' | awk -F: '{print $2}')
        display="${ip}:0.0"
    else
        display="${DISPLAY}"
    fi
}
export -f get_display


