#!/bin/bash

set -e

if [ $(hash docker) ]; then
    echo "Docker command not found!"
    exit
fi

this=$(basename ${0})
image_tag="ebelter/${this}:latest"
image_sha=$(docker images -q ${image_tag})
if [ -z "${image_sha}" ]; then
    docker pull "${image_tag}"
    image_sha=$(docker images -q ${image_tag})
fi

DOCKER_ARGS=()
UNKNOWN_ARGS=()
while [[ $# -gt 0 ]]; do
    key="$1"
    case $key in
        -p|--port)
            PORT="$2"
            DOCKER_ARGS+=("-p" "${PORT}:${PORT}" "--expose" "${PORT}")
            unset PORT
            shift # past argument
            shift # past value
            ;;
        *)
        UNKNOWN_ARGS+=("$1")
        shift
  esac
done

if [[ ${#UNKNOWN_ARGS} -gt 0 ]]; then
    echo "Unknown args: ${UNKNOWN_ARGS[@]}"
    exit 2
fi

me=$(whoami)
docker run -it --rm -h "${this}" ${DOCKER_ARGS[@]} -v "/Users/${me}/dev/:/home/ebelter/dev" -v "/Users/${me}/.ssh:/home/ebelter/.ssh" "${image_sha}" /bin/bash
