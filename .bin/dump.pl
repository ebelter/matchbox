#!/usr/bin/env perl
#
# convert to python
# python reads perl storables:
# pip install storable

use strict;
use warnings 'FATAL';

use Data::Dumper;
use JSON;
use File::Slurp 'slurp';
use List::Util 'any';
use Path::Class;
use Storable;
use YAML;

my $usage = <<USAGE;
dump.pl <FILE> <AS>
  file type is JSON, YAML #FIXME ADD STORABLE!
  AS default is 'perl', but could be 'json' or 'yaml'
USAGE

die "No file given!" if not @ARGV;
die "No file given!" if not @ARGV;
my $file = file($ARGV[0]);
die "File $file does not exist!" if not -s $file;

my ($type) = $file->stringify =~ /\.(\w+)/;
my @valid_types = (qw/ json yaml yml /);
die sprintf('No/invalid file type %s. Valid types: %s', $type, join(', ', @valid_types)) if not $type or not any { $type eq $_ } @valid_types;

my $contents = slurp($file->stringify); # should croak on error
die "No contents in $file" if not defined $contents;

my $as = $ARGV[1] || 'yaml';
die "Invalid as: $as" if not any { $as eq $_ } (qw/ var yaml /);

if ( $type eq 'json' ) {
if ( $type eq 'json' ) {
    my $data = from_json($contents);
    my $as = $ARGV[1] || 'yaml';
    ( $as eq 'var' ) ? print Dumper($data) : print Dump($data);
}
else { # yaml
    my $as = $ARGV[1] || 'var'; # otherwise just cat the file
    ( $as eq 'var' ) ? print Dumper( Load($contents) ) : print $contents;
}

exit 0;
