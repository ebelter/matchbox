#!

#/ FUNCTIONS /#
_start() {
    echo "Start server"
    _start_server
    if [[ $? != 0 ]]; then echo Failed to start synergy server!; return $?; fi
    echo "[Re]start client"
    _stop_client
    _start_client
    if [[ $? != 0 ]]; then echo Failed to start synergy client!; return $?; fi
    echo "Synergy start!"
    return 0;
}

_stop() {
    echo "Stop client"
    _stop_client
    echo "Stop server"
    _stop_server
    echo "Synergy stop!"
    return 0
}

_start_client() {
    PIDS=$(ps -eo pid,args | grep synergyc | sed 's/^\s+//' | grep linus260 | grep -v grep)
    if [[ ! -n $PIDS ]]; then
        synergyc linus260 &> /dev/null;
	return $?
    fi
    return 0
}

_stop_client() {
    PIDS=$(ps -eo pid,args | grep synergyc | sed 's/^\s+//' | grep linus260 | grep -v grep | awk '{print $1}')
    if [[ -n $PIDS ]]; then
        echo Killing client pids: ${PIDS}
        kill ${PIDS}
    fi
    return 0
}

_start_server() {
    PIDS=$(ssh -o ConnectTimeout=5 linus260 ps -eo pid,args | grep synergys | grep -v grep | awk '{print $1}')
    if [[ ! -n $PIDS ]]; then
        ssh -o ConnectTimeout=5 linus260 synergys -c /gscuser/ebelter/.synergy/.left
	return $?
    fi
    return 0
}

_stop_server() {
    PIDS=$(ssh -o ConnectTimeout=5 linus260 ps -eo pid,args 2>/dev/null | grep synergys | sed 's/^\s+//' | grep -v grep | awk '{print $1}')
    if [[ -n $PIDS ]]; then
        echo Killing server pids: ${PIDS}
        ssh -o ConnectTimeout=5 linus260 kill ${PIDS}
	return $?
    fi
    return 0
}
#//#

if [ "$1" == "start" ]; then
    _start
elif [ "$1" == "stop" ]; then
    _stop
elif [ $1 ]; then
    echo "Option '$1' is invalid. Please indicate 'start' or 'stop'."
else
    echo "Please indicate start or stop."
fi

exit 0;

