#!/bin/bash

declare listening
listening=$(defaults read org.macosforge.xquartz.X11 nolisten_tcp)
if [ -z "${listening}" ] || [ "${listening}" -eq 0 ]; then
    defaults write org.macosforge.xquartz.X11 nolisten_tcp 0
fi

declare ip
declare route
if [ "$(uname -s)" = "Darwin" ]; then
    # /sbin/ifconfig $( /sbin/route get default | awk '/interface/{print $2}') | awk '/inet /{print $2}'
    route=$(/sbin/route get default | awk '/interface/{print $2}')
    ip=$(/sbin/ifconfig "${route}" | awk '/inet /{print $2}')
else
    # /bin/ip addr show dev $( /sbin/route -n | grep ^0.0.0.0 | awk '{print $NF}' ) | awk '/ inet /{print $2}' | sed -E 's/\/[[:digit:]]+//g'; fi)
    route=$(/sbin/route -n | grep ^0.0.0.0 | awk '{print $NF}')
    ip=$(/bin/ip addr show dev "${route}" | awk '/ inet /{print $2}' | sed -E 's/\/[[:digit:]]+//g')
fi
echo "ROUTE: ${route}"
echo "IP:    ${ip}"

xhost + "${ip}"
