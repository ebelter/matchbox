# .bash_profile

# MGI bash_profile
if [ -z "${MGI_NO_GAPP}" ]; then # ok to source GAPP bash_profile even tho it is terrible
    [ -f /gapp/noarch/share/login/gapp.profile ]  && . /gapp/noarch/share/login/gapp.profile
else
    [ -f /opt/lsf9/conf/profile.lsf ] && . /opt/lsf9/conf/profile.lsf
fi

# .bashrc
. "${HOME}/.bashrc"
